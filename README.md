virt-backup
---

```
usage: virt-backup [-h] [--config CONFIG] --server SERVER

Backup virtual machines from LibVirt

options:
  -h, --help            show this help message and exit
  --config CONFIG, -c CONFIG
                        Config file to use
  --server SERVER, -s SERVER
                        Server to backup
```

