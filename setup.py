import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='virtbak',  
    version='1.0',
    author="Oytun OZDEMIR",
    author_email="oytunozdemir@yandex.com",
    description="Backup virtual machines from LibVirt",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/oytunistrator/virt-backup",
    packages=["virtbak", "virtbak.virt"],
    entry_points = {
        "console_scripts": ['virtbak = virtbak.virtbak:virtBak'],
    },
    install_requires=[
        "attrs",
        "libvirt-python",
        "lxml",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
